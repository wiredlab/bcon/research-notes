# Funding opportunities


## University Nanosatellite Program (UNP) Mission Concepts Summer Series

* Call for Proposals released Jan 4

https://www.nasa.gov/smallsat-institute/nasa-air-force-space-force-help-universities-compete-to-build-small-satellites

Solicitation site: https://universitynanosat.org/solicitation/

**Feb 3 proposal deadline**


## ISS National Lab Research Announcement

https://www.issnationallab.org/research-on-the-iss/solicitations/nlra2023-5/

Requirement for proposal: 
- Proposals for flight projects must include a statement defining how the scientific aims will benefit from being executed in space and why the proposed investigation can only be performed in space

Application procress/ time line for process: 
This research announcement will follow a two-step proposal submission process. Before being invited to submit a full
proposal, all interested investigators must submit a Step 1: Concept Summary for review; Concept Summaries must be
submitted by end of day on **February 28, 2023**. Step 2: Full Proposals (from those invited to submit) will be due by end
of day **May 3, 2023**.

Amount: $600,000

## Amateur Radio Digital Communications

Eligibility and Application Process on website- 
https://www.ampr.org/apply/
 
Amount not listed- 
**Deadline(s)**: 
- February 1
- April 1
- July 1
- October 1 + Scholarship Applications Due

## Nasa (maybe) 
https://www.nasa.gov/directorates/heo/scan/communications/outreach/grants

# Other Assoiations that might have something good 

## ASN 
https://www.asn-online.org/grants/additional.aspx

